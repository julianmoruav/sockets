#-------------------------------------------------------------#
# Minimal UDP Client that sends strings to UDP server         #
#     Redes de Computadores I-2020 UCR                        #
#-------------------------------------------------------------#

import socket

def main():
    print("################################")
    print("[Info] Setting up UDP client.")

    # Create socket for IPv4 and TCP protocols:
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    print("[Info] Created socket for UDP connection over IP.\n")

    # while loop will stop only if client types 'STOP':
    active = True
    while active:
        # Take input string from user:
        print("[Client] Insert the string that you want to send to the server:")
        print("[Client] (To finish sending characters to the server type 'STOP'.")
        dataOut = raw_input()

        # Make sure input string is not empty:
        while not dataOut:
            dataOut = raw_input()

        # Check if user string is something to be sent:
        if dataOut != "STOP":
            # Send string to the server:
            s.sendto(dataOut, ("127.0.0.1", 9001))
            print("[Client] String sent to server at IP address 127.0.0.1, port 9001.")

            # Receive server's response:
            dataIn, addr = s.recvfrom(1024)
            print("[Client] Got back from server: " + dataIn + "\n")

        # If user string is STOP then the socket must be closed
        else:
            # Send SHUTDOWN command to the server:
            s.sendto("SHUTDOWN", ("127.0.0.1", 9001))
            print("[Client] SHUTDOWN command sent to server at IP address 127.0.0.1, port 9001.")

            s.close() # closes the actual socket on the client's devivce.
            print("[Client] Closed socket. Client can no longer send data.")
            active = False

    print("[Info] UDP demo with Python Sockets is finished.")


if __name__ == "__main__":
    main()
