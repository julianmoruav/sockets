#-------------------------------------------------------------#
# Minimal TCP Server that converts string to upper case only  #
#     Redes de Computadores I-2020 UCR                        #
#-------------------------------------------------------------#

import socket

def main():
    print("################################")
    print("[Info] Setting up TCP server.")

    # Create a transaction counter:
    count = 0

    # Create a socket for IPv4 and TCP protocols; this is the Welcome Socket:
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    print("[Info] Created socket for TCP connection over IP.")

    # Associate an IP address and a port to the socket:
    s.bind(("127.0.0.1", 4200))
    print("[Info] Socket binded to IP address 127.0.0.1, port 4200. (Loopback).")

    # Put the socket into listening mode:
    s.listen(1) # one connection at a time.
    print("[Info] Welcome socket is listening; TCP server is ready.")

    # Connect to client; this creates a Connection Socket "c":
    c, addr = s.accept()
    connected = True
    print("[Server] Established connection with client " + str(addr))

    # while loop will stop only if client is disconnected:
    while connected:
        # Receive Information:
        dataIn = c.recv(1024)

        # Check for valid received data:
        if dataIn:
            # Count another transaction:
            count = count + 1
            print("[Server] count = " + str(count))

            # Perform operations on information:
            dataOut = dataIn.upper()

            # Send response to client:
            c.send(dataOut)

        # If no data is received then the client closed the connection on its end:
        # Shutting down the server because the connection was ended is not common practice, in fact, a server should remain online waiting for other clients. However, for this implementation the only client is now offline, so the servers' sockets are no longer needed and can be closed.
        # Doing this from within this program saves us the extra work needed to kill from the terminal the process that handles the socket. This is a trick and it is not part of the TCP protocol.
        else:
            # Close connection from server to client:
            c.close()
            print("[Server] Closed connection with client " + str(addr))
            connected = False
            s.close()

    print("[Info] TCP demo with Python Sockets is finished.")


if __name__ == "__main__":
    main()
