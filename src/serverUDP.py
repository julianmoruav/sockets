#-------------------------------------------------------------#
# Minimal UDP Server that converts string to upper case only  #
#     Redes de Computadores I-2020 UCR                        #
#-------------------------------------------------------------#

import socket

def main():
    print("################################")
    print("[Info] Setting up UDP server.")

    # Create a transaction counter:
    count = 0

    # Create a socket for IPv4 and UDP protocols:
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    print("[Info] Created socket for UDP connection over IP.")

    # Associate an IP address and a port to the socket:
    s.bind(("127.0.0.1", 9001))
    print("[Info] Socket binded to IP address 127.0.0.1, port 9001. (Loopback).")
    print("[Info] UDP server is ready.")

    # while loop will stop only if client sends the SHUTDOWN command:
    open = True
    while open:
        # Receive Information and sender's address:
        dataIn, addr = s.recvfrom(1024)

        # Check if the client is giving the SHUTDOWN command:
        if dataIn != "SHUTDOWN":
            # Count another transaction:
            count = count + 1
            print("[Server] count = " + str(count))

            # Perform operations on information:
            dataOut = dataIn.upper()

            # Send response to client:
            s.sendto(dataOut, addr)

        # SHUTDOWN command closes the UDP server:
        # Shutting down a server is a task for the servers' administrators, Clients don't usually send shutdown commands to a server. However, for this implementation the only client is now offline, so the server's socket is no longer needed and can be closed.
        # Doing this from within this program saves us the extra work needed to kill from the terminal the process that handles the socket. This is a trick and it is not part of the UDP protocol.
        else:
            print("[Server] Received SHUTDOWN command.")
            s.close()
            print("[Server] Closed socket, no longer receving data from any client.")
            open = False

    print("[Info] UDP demo with Python Sockets is finished.")


if __name__ == "__main__":
    main()
