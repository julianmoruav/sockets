#-------------------------------------------------------------#
# Minimal TCP Client that sends strings to TCP server         #
#     Redes de Computadores I-2020 UCR                        #
#-------------------------------------------------------------#

import socket

def main():
    print("################################")
    print("[Info] Setting up TCP client.")

    # Create socket for IPv4 and TCP protocols:
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    print("[Info] Created socket for TCP connection over IP.")

    # Connect to server on loopback
    s.connect(("127.0.0.1", 4200))
    connected = True
    print("[Client] Connected to TCP server at IP address 127.0.0.1, port 4200.\n")

    # while loop will stop only if client types 'STOP':
    while connected:
        # Take input string from user:
        print("[Client] Insert the string that you want to send to the server:")
        print("[Client] (To finish the connection with server type 'STOP'.)")
        dataOut = raw_input()

        # Make sure input string is not empty:
        while not dataOut:
            dataOut = raw_input()

        # Check if user string is something to be sent
        if dataOut != "STOP":
            # Send string to the server:
            s.send(dataOut)

            # Receive server's response:
            dataIn = s.recv(1024)
            print("[Client] Got back from server: " + dataIn + "\n")
        # If user string is STOP then the connection must be closed
        else:
            # Close connection from client to server:
            s.shutdown(socket.SHUT_RDWR) # lets server know the connection has been closed.
            s.close() # closes the actual socket on the client's device.
            print("[Client] Closed connection with server at IP address 127.0.0.1, port 4200.")
            connected = False

    print("[Info] TCP demo with Python Sockets is finished.")


if __name__ == "__main__":
    main()
