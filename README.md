# sockets

Minimal TCP and UDP servers and clients. Implemented as loopbacks.
The servers receive strings sent from the clients and proceed to send them back with all the letters capitalized. Implemented using Python 2.7 and Python's `socket` library.

Asignación 2.
Redes de Computadores.
Universidad de Costa Rica.

# How to run
Two terminals are requiered at the same time, one for the server and one for the client.
The server must be started before the client, otherwise the connection will fail.
The server's terminal will notify you when the server is ready for a client to send data.

- Start up the TCP Server:
`python ./src/serverTCP.py`

- Start up the UDP Server:
`python ./src/serverUDP.py`

Once the server is up, start up the client. For TCP, the client will attempt a connection on its own and both terminals will notify you when a connection is active. For UDP, the communication is connection-less.

- Start up the TCP Client:
`python ./src/clientTCP.py`

- Start up the UDP Client:
`python ./src/clientUDP.py`

Type strings into the client's terminal to have them manipulated by the server.
Type the word 'STOP' to shutdown the server and client at the same time.


# References

1. https://docs.python.org/2/library/socket.html
2. https://realpython.com/python-sockets/
3. https://www.tutorialspoint.com/python/python_networking.htm
4. https://wiki.python.org/moin/UdpCommunication
